**************************************
**Welcome to ETRACK's Documentation!**
**************************************


**Getting Started**
###################


This documentation describes the data groups and data points used in ETRACK Systems.
	
Visit ETRACK's website `here <http://etrack.africa/>`_


**How ETRACK connects with your Electricity meter**
	
.. figure:: ./images/etrackgateway.png
	:width: 400px
   	:align: center
    	:height: 400px
    	:alt: alternate text




.. toctree::
   :hidden:
   
   manifest
   devices/converter_combox
   devices/conext_xw
   devices/conext_sw
   devices/conext_mppt60
   devices/voltronic
   devices/eastron_smart
   devices/epsolar
   devices/eastron_sdmc   













