**Manifest**
############

.. code-block:: JSON

   {
      "settings": {
        "site_details": {
          "d_comm": "2019-09-26",    
          "addr": "21 Ribadu Rd, Ikoyi, Lagos",
          "plc_d_comm": "2019-09-26",    
          "site_code": "",
          "space_code": "chm-0001",
          "name": "House 9, The Address Homes",
          "desc": "residence estate"
      },
      "site_loc": {
        "lat": "6.444661",    
        "city": "Ikoyi",
        "state": "Lagos",
        "long": "3.418113",	
        "country": "Nigeria"
      },
      "utils": {
        "pub_ipv4": "",
        "priv_ipv4": ""
      },
      "devices": {
        "all": [
          {
            "sn": "1236009",
            "type": "eastron_smartx96_5j",
            "info":{
                "device_type": "meter",
                "device_info": "X96-5J v1.0",
                "pt1": "230.0",
                "pt2": "230.0",
                "ct1": "100.0",
                "ct2": "5.0",
                "conn_type": "3p4w",
                "sn": "1236009"
            },
            "addr": "1.0",
            "data_opt": {
                "load": true,
                "pv": false, 
                "batt": false, 
                "grid": false, 
                "gen": false
            },
            "name": "X96-5J v1.0"
          }
        ],
        "num": 1
      },
      "etrack_hw": {
        "is_enabled": true,
        "has_camera": false,
        "name": "etrack v1.0.0",
        "plc_interface": "modbus_tcp",
        "plc_serial": "/dev/ttyUSB0",
        "plc_ipv4": "192.168.1.200",
        "plc_brand": "eastron",
        "plc_port": "502"
      },
      "data_opt": {
        "load": true,
        "pv": false, 
        "batt": false, 
        "grid": false, 
        "gen": false
      },
      "manu_details": {
        "name": "inventech llc, nigeria",
        "email": "info@inventech.ng"
      },
      "gateway": {
        "is_enabled": true,
        "d_comm": "2019-09-26",		
        "name": "ETG-I",
        "priv_ipv4": "",
        "firmware": "etrack_monitor v1.0.0",
        "logging_interval": "5min",
        "devices": {
          "all": [
            {
              "sn": "1236009",
              "type": "eastron_smartx96_5j",
              "info":{
                  "device_type": "meter",
                  "device_info": "X96-5J v1.0",
                  "pt1": "230.0",
                  "pt2": "230.0",
                  "ct1": "100.0",
                  "ct2": "5.0",
                  "conn_type": "3p4w",
                  "sn": "1236009"
              },
              "addr": "1.0",
              "data_opt": {
                  "load": true,
                  "pv": false, 
                  "batt": false, 
                  "grid": false, 
                  "gen": false
              },
              "name": "X96-5J v1.0"
            }
          ],
          "num": 1
          },
        "pub_ipv4": "",
        "has_camera": false,
        "remote_access": "enabled",
        "mac_address": "00:0F:00:BC:0F:ED",
        "id": "d1ea517260cbbcc7a605fbc2070dc379fe4ac953"
      },
      "installer_details": {
        "phone": "+234 8185476560",
        "name": "inventech llc"
     	 }
   	 }
    }


=================== ===============================
Settings		Meaning
=================== =============================== 
d_comm			date commissioned
addr			address
plc_d_comm		device date commissioned
lat			latitude
long			longitude	
pub_ipv4		public IP address
priv_ipv4		private IP address
sn			serial number
pt1 			phase transformer 1
pt2			phase transformer 2
ct1			current transformer 1
ct2			current transformer 2
conn_type		connection type
data_opt		data options
plc_interface		device interface
plc_serial		serial device
plc_ipv4		device IP address
plc_brand		device brand






=================== ===============================

