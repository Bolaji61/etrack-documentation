**Schneider Electric Conext MPPT60**
####################################

**Device type: conext mppt60**



.. image:: ../images/conext_mppt60.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: Image of conext_mppt60

**Device communication with ETRACK Gateway**

.. image:: ../images/conext_mppt60_line.png
	:width: 800px
   	:align: center
    	:height: 120px
    	:alt: alternate text


**Datapoints**
--------------

.. code-block:: JSON

   {
  "conext_mppt60": [
    {
      "sn": "",
    "addr": "",
    "batt": {
        "idv": "",
        "idc": "",
        "idp": "",
        "bt": "",
        "odv": "",
        "odc": "",
        "odp": "",
        "dpop": "",
        "nbv": "",
        "bbc": ""
    },
    "pv": {
        "idv": "",
        "idc": "",
        "idp": "",
        "odv": "",
        "odc": "",
        "odp": "",
        "dpop": "",
        "pieh": "",
        "piet": "",
        "piew": "",
        "piem": "",
        "piey": "",
        "piel": "",
        "poeh": "",
        "poet": "",
        "poew": "",
        "poem": "",
        "poey": "",
        "poel": ""
    },
    "faults": {
        "n": "",
        "active": false,
        "fault": []
    },
    "warns": {
        "n": "",
        "active": true,
        "warn": []
    	}
      }
     ]
    }


**Battery**
-----------
	
Data group: batt
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
idv (str)			Input DC Voltage			V
idc (str)			Input DC current			A
idp (str)			Input DC power				W
bt (str)			Battery Temperature			deg C
odv (str)			Output DC Voltage			V
odc (str)			Output DC Current			A
odp (str)			Output DC Power				W
dpop (str)			DC Power Output Percentage		%
nbv (str)			Nominal Battery Voltage			V
bbc (str)			Battery Bank Capacity			Ah

=================== ========================================== ====================

**Photo Voltaic**
-------------------

Data group: pv
--------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
idv (str)			Input DC Voltage			V
idc (str)			Input DC current			A
idp (str)			Input DC power				W
odv (str)			Output DC Voltage			V
odc (str)			Output DC Current			A
odp (str)			Output DC Power				W
dpop (str)			DC Power Output Percentage		%
pieh (str)			PV Input Energy Hour			kWh
piet (str)			PV Input Energy Today			kWh
piew (str)			PV Input Energy Week			kWh
piem (str)			PV Input Energy Month			kWh
piey (str)			PV Input Energy Year			kWh
piel (str)			PV Input Energy Lifetime		kWh
poeh (str)			PV Output Energy Hour			kWh
poet (str)			PV Output Energy Today			kWh
poew (str)			PV Output Energy Week			kWh
poem (str)			PV Output Energy Month			kWh
poey (str)			PV Output Energy Year			kWh
poel (str)			PV Output Energy Lifetime		kWh

=================== ========================================== ====================

**Faults**
----------

Data group: faults
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			""
fault (arr)		Array of faults				[""]

=================== ========================================== =============================

**Warns**
----------

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			" "
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================

