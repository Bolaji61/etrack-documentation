**Schneider Electric Converter ComBox**
#######################################

**Device type: converter combox**


The Converter combox is a powerful communication and monitoring device for installers and operators of Conext solar systems. It combines all functions of the different converter devices.

.. image:: ../images/conext_combox.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: alternate text

**Device communication with ETRACK Gateway**

.. image:: ../images/conext_combox_line.png
	:width: 600px
   	:align: center
    	:height: 200px
    	:alt: alternate text



**Datapoints**
--------------

.. code-block:: JSON

   {
   "converter_combox": [
    {
      "sn": "",
      "addr": "",
      "batt": {
        "bt": "",
        "bv": "",
        "bcn": "",
        "bpn": "",
        "dciefbh": "",
        "dciefbt": "",
        "dciefbw": "",
        "dciefbm": "",
        "dciefby": "",
        "dciefbl": "",
        "dcoetbh": "",
        "dcoetbt": "",
        "dcoetbw": "",
        "dcoetbm": "",
        "dcoetby": "",
        "dcoetbl": "",
        "dccp": "",
        "dccc": "",
        "dcip": ""
      },
      "batt_bank": {
        "bbv": [],
        "bbc": [],
        "bbt": [],
        "bbcc": [],
        "bbcp": [],
        "bbic": [],
        "bbip": [],
        "bbceh": [],
        "bbcet": [],
        "bbcew": [],
        "bbcem": [],
        "bbcey": [],
        "bbcel": [],
        "bbieh": [],
        "bbiet": [],
        "bbiew": [],
        "bbiem": [],
        "bbiey": [],
        "bbiel": [],
        "bbsoc": [],
        "bbcr": [],
        "bbtur": [],
        "bblrt": [],
        "bbtip": "",
        "bbtcp": ""
      },
      "pv": {
        "pieh": "",
        "piet": "",
        "piew": "",
        "piem": "",
        "piey": "",
        "piel": "",
        "apv": "",
        "tpc": "",
        "paoeh": "",
        "paoet": "",
        "paoew": "",
        "paoem": "",
        "paoey": "",
        "paoel": "",
        "ptp": "",
        "ptpm": "",
        "pteh": "",
        "ptet": "",
        "ptew": "",
        "ptem": "",
        "ptey": "",
        "ptel": ""
      },
      "gen": {
        "gs": "",
        "agp": "",
        "gieh": "",
        "giet": "",
        "giew": "",
        "giem": "",
        "giey": "",
        "giel": "",
        "agv": "",
        "agf": "",
        "tgc": "",
        "grs": ""
      },
      "grid": {
        "gv": "",
        "gf": "",
        "gic": "",
        "gip": "",
        "gop": "",
        "gieh": "",
        "giet": "",
        "giew": "",
        "giem": "",
        "giey": "",
        "giel": "",
        "goeh": "",
        "goet": "",
        "goew": "",
        "goem": "",
        "goey": "",
        "goel": "",
        "tgoc": "",
        "gnp": ""
      },
      "load": {
        "lop": "",
        "lp": "",
        "lv": "",
        "lf": "",
        "lc": "",
        "aleh": "",
        "alet": "",
        "alew": "",
        "alem": "",
        "aley": "",
        "alel": "",
        "lpm": ""
      },
      "faults": {
        "n": "",
        "active": false,
        "fault": []
      },
      "warns": {
        "n": "",
        "active": true,
        "warn": []
      }
    }
   ]
  }


**Battery**
-----------
	
Data group: batt
----------------

=================== ============================================ ====================
Datapoints          	      Meaning                               Unit
=================== ============================================ ====================
bt (str)		Battery Temperature				deg C
bv (str)		Battery Voltage					V
bcn (str)		Battery Current Net				A
bpn (str)		Battery Power Net				W
dciefbh (str)		DC Input Energy From Battery (Hour)		kWh
dciefbt (str)		DC Input Energy From Battery (Today)		kWh
dciefbw (str)		DC Input Energy From Battery (Week)		kWh
dciefbm (str)		DC Input Energy From Battery (Month)		kWh
dciefby (str)		DC Input Energy From Battery (Year)		kWh
dciefbl (str)		DC Input Energy From Battery (Lifetime)		kWh
dcoetbh (str)		DC Output Energy To Battery (Hour)		kWh
dcoetbt (str)		DC Output Energy To Battery (Today)		kWh
dcoetbw (str)		DC Output Energy To Battery (Week)		kWh
dcoetbm (str)		DC Output Energy To Battery (Month)		kWh
dcoetby (str)		DC Output Energy To Battery (Year)		kWh
dcoetbl (str)		DC Output Energy To Battery (Lifetime)		kWh
dccp (str)		DC Charging Power				W
dccc (str)		DC Charging Current				A
dcip (str)		DC Inverting Power				W


=================== ============================================ ====================



**Battery Bank**
----------------

Data group: batt_bank
---------------------

=================== ================================================== ====================
Datapoints          	     		 Meaning                           Unit
=================== ================================================== ====================
bbv (arr)			Battery bank Voltage				V
bbc (arr)			Battery bank Current				A
bbt (arr)			Battery bank Temperature			deg C
bbcc (arr)			Battery bank charging current			A
bbcp (arr)			Battery bank charging power			W
bbic (arr)			Battery bank inverting current			A
bbip (arr)			Battery bank inverting power			W
bbceh (arr)			Battery bank charging energy hour		kWh
bbcet (arr)			Battery bank charging energy today		kWh
bbcew (arr)			Battery bank charging energy week		kWh
bbcem (arr)			Battery bank charging energy month		kWh
bbcey (arr)			Battery bank charging energy year		kWh
bbcel (arr)			Battery bank charging energy lifetime		kWh
bbieh (arr)			Battery bank inverting energy hour		kWh
bbiet (arr)			Battery bank inverting energy today		kWh
bbiew (arr)			Battery bank inverting energy week		kWh
bbiem (arr)			Battery bank inverting energy month		kWh
bbiey (arr)			Battery bank inverting energy year		kWh
bbiel (arr)			Battery bank inverting energy lifetime		kWh
bbsoc (arr)			Battery bank state of charge			%
bbcr (arr)			Battery bank capacity remaining			Ah
bbtur (arr)			Battery bank time until recharge		s
bblrt (arr)			Battery bank last recharge time			s
bbtip (str)			Battery bank total inverting power		W
bbtcp (str)			Battery bank total charging power		W






=================== ================================================== ====================


**Photo Voltaic**
-------------------

Data group: pv
--------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
pieh (str)			PV input energy hour			kWh
piet (str)			PV input energy today			kWh
piew (str)			PV input energy week			kWh
piem (str)			PV input energy month			kWh
piey (str)			PV input energy year			kWh
piel (str)			PV input energy lifetime		kWh
apv (str)			Average PV voltage			V
tpc (str)			Total PV current			A
paoeh (str)			PV AC output energy hour		kWh
paoet (str)			PV AC output energy today		kWh
paoew (str)			PV AC output energy week		kWh
paoem (str)			PV AC output energy month		kWh
paoey (str)			PV AC output energy year		kWh
paoel (str)			PV AC output energy lifetime		kWh
ptp (str)			PV total power				W
ptpm (str)			PV total power max			W
pteh (str)			PV total energy hour			kWh
ptet (str)			PV total energy today			kWh
ptew (str)			PV total energy week			kWh
ptem (str)			PV total energy month			kWh
ptey (str)			PV total energy year			kWh
ptel (str)			PV total energy lifetime		kWh


=================== ========================================== ====================


**Generator**
-------------

Data group: gen
---------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
gs (str)			Gen state				" "
agp (str)			AC Gen power				W
gieh (str)			Gen input energy hour			kWh
giet (str)			Gen input energy today			kWh
giew (str)			Gen input energy week			kWh
giem (str)			Gen input energy month			kWh
giey (str)			Gen input energy year			kWh
giel (str)			Gen input energy lifetime		kWh
agv (str)			Average gen voltage			V
agf (str)			Average gen frequency			Hz
tgc (str)			Total gen current			A
grs (str)			Gen running since			s

=================== ========================================== ====================



**Grid (Light source coming from PHCN)**
-----------------------------------------

Data group: grid
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
gv (str)			Grid voltage				V
gf (str)			Grid frequency				Hz
gic (str)			Grid input current			A
gip (str)			Grid input power			W
gop (str)			Grid output power			W
gieh (str)			Grid input energy hour			kWh
giet (str)			Grid input energy today			kWh
giew (str)			Grid input energy week			kWh
giem (str)			Grid input energy month			kWh
giey (str)			Grid input energy year			kWh
giel (str)			Grid input energy lifetime		kWh
goeh (str)			Grid output energy hour			kWh
goet (str)			Grid output energy today		kWh
goew (str)			Grid output energy week			kWh
goem (str)			Grid output energy month		kWh
goey (str)			Grid output energy year			kWh
goel (str)			Grid output energy lifetime		kWh
tgoc (str)			Total grid output current		A
gnp (str)			Grid net power				W


=================== ========================================== ====================


**Load**
----------

Data group: load
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
lop (str)			Load output power			W
lp (str)			Load power				W
lv (str)			Load voltage				V
lf (str)			Load frequency				Hz
lc (str)			Load current				A
aleh (str)			AC load energy hour			kWh
alet (str)			AC load energy today			kWh
alew (str)			AC load energy week			kWh
alem (str)			AC load energy month			kWh
aley (str)			AC load energy year			kWh
alel (str)			AC load energy lifetime			kWh
lpm (str)			Load power max				W

=================== ========================================== ====================

**Faults**
----------

Data group: faults
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			" "
fault (arr)		Array of faults				[""]

=================== ========================================== =============================


**Warns**
----------

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			" "
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================

