**Eastron SDM530C**
####################

**Device type: eastron_sdm530c**


.. image:: ../images/eastron530c.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: alternate text

**Device communication with ETRACK Gateway**

.. image:: ../images/eastron_sdm_line.png
	:width: 600px
   	:align: center
    	:height: 200px
    	:alt: alternate text


**Datapoints**
--------------

.. code-block:: JSON

   {
  "data": {
   "timestamp": 1562172417236,
   "compute": {
     "eastron_sdm530c": {
       "load": [
         {
           "use": "instant",
           "datapoint": "tape"
         },
         {
           "use": "change",
           "datapoint": "tace"
         },
         {
           "use": "change",
           "datapoint": "tree"
         }
       ]
     }
   },
   "devices": {
     "eastron_sdm530c": [
       {
         "load": {
           "l1tnv": "",
           "l2tnv": "",
           "l3tnv": "",
           "l1c": "",
           "l2c": "",
           "l3c": "",
           "l1acp": "",
           "l2acp": "",
           "l3acp": "",
           "l1app": "",
           "l2app": "",
           "l3app": "",
           "l1rep": "",
           "l2rep": "",
           "l3rep": "",
           "l1pf": "",
           "l2pf": "",
           "l3pf": "",
           "l1pa": "",
           "l2pa": "",
           "l3pa": "",
           "apv": "",
           "ac": "",
           "tc": "",
           "tacp": "",
           "tapp": "",
           "trep": "",
           "tpf": "",
           "tpa": "",
           "fq": "",
           "itace": "",
           "otace": "",
           "itree": "",
           "otree": "",
           "tacpd": "",
           "macpd": "",
           "appd": "",
           "mappd": "",
           "trepd": "",
           "mrepd": "",
           "l1tl2v": "",
           "l2tl3v": "",
           "l3tl1v": "",
           "alv": "",
           "nlc": "",
           "l1cd": "",
           "l2cd": "",
           "l3cd": "",
           "l1mcd": "",
           "l2mcd": "",
           "l3mcd": "",
           "tace": "",
           "tree": "",
           "tape": ""
         },
         "faults": {
           "active": false,
           "fault": [],
           "n": 0
         },
         "sn": "1232892",
         "warns": {
           "active": false,
           "warn": [],
           "n": 0
         },
         "addr": "1.0"
        }
       ]
     }
    }



**Load**
--------

Data group: load
-----------------

=================== ======================================================== ============================= ==============

Datapoints          	      Meaning                               						Unit
=================== ======================================================== ============================= ==============
l1tnv (str)			Line 1 to Neutral Volts								V
l2tnv (str)			Line 2 to Neutral Volts								V
l3tnv (str)			Line 3 to Neutral Volts								V
l1c (str)			Line 1 current									A
l2c (str)			Line 2 current									A
l3c (str)			Line 3 current									A
l1acp (str)			Line 1 active power								W
l2acp (str)			Line 2 active power								W
l3acp (str)			Line 3 active power								W
l1app (str)			Line 1 apparent power								VA
l2app (str)			Line 2 apparent power								VA
l3app (str)			Line 3 apparent power								VA
l1rep (str)			Line 1 reactive power								VAr
l2rep (str)			Line 2 reactive power								VAr
l3rep (str)			Line 3 reactive power								VAr
l1pf (str)			Line 1 power factor								None
l2pf (str)			Line 2 power factor								None
l3pf (str)			Line 3 power factor								None
l1pa (str)			Line 1 phase angle								Degrees
l2pa (str)			Line 2 phase angle								Degrees
l3pa (str)			Line 3 phase angle								Degrees
apv (str)			Average phase voltage								V
ac (str)			Average current									A
tc (str)			Total current									A
tacp (str)			Total active power								W
tapp (str)			Total apparent power								VA
trep (str)			Total reactive power								VAr
tpf (str)			Total power factor								None
tpa (str)			Total phase angle								Degrees
fq (str)			Frequency									Hz
itace (str)			Input total active energy							kWh
otace (str)			Output total active energy							kWh
itree (str)			Input total reactive energy							kVArh
otree (str)			Output total reactive energy							kVArh
tacpd (str)			Total active power demand							W
macpd (str)			Max active power demand								W
appd (str)			Apparent power demand								VA
mappd (str)			Max apparent power demand							VA
trepd (str)			Total reactive power demand							VAr
mrepd (str)			Max reactive power demand							VAr
l1tl2v (str)			Line 1 to Line 2 volts								V
l2tl3v (str)			Line 2 to Line 3 volts								V
l3tl1v (str)			Line 3 to Line 1 volts								V
alv (str)			Average line volts								V
nlc (str)			Null line current								A
l1cd (str)			Line 1 current demand								A
l2cd (str)			Line 2 current demand								A
l3cd (str)			Line 3 current demand								A
l1mcd (str)			Line 1 max current demand							A
l2mcd (str)			Line 2 max current demand							A
l3mcd (str)			Line 3 max current demand							A
tace (str)			Total active energy								kWh
taceh (str)			Total active energy hour (auto-generated) 	Hourly Active energy		kWh
taced (str)			Total active energy today (auto-generated) 	Daily Active Energy 		kWh
tacew (str)			Total active energy week (auto-generated) 	Weekly Active Energy 		kWh
tacem (str)			Total active energy month (auto-generated) 	Monthly Active Energy 		kWh
tacey (str)			Total active energy year (auto-generated) 	Yearly Active Energy 		kWh
tree (str)			Total reactive energy								kVArh
treeh (str)			Total reactive energy hour (auto-generated) 	Hourly Reactive energy 		kVArh	
treed (str)			Total Reactive energy today (auto-generated) 	Daily Reactive energy 		kVArh
treew (str)			Total Reactive energy week (auto-generated) 	Weekly Reactive energy 		kVArh
treem (str)			Total Reactive energy month (auto-generated) 	Monthly Reactive energy 	kVArh
treey (str)			Total Reactive energy year (auto-generated) 	Yearly Reactive energy 		kVArh
tape (str)			Total apparent energy								kVAh
tapeh (str)			Total apparent energy hour (auto-generated) 	Hourly Apparent energy 		kVAh
taped (str)			Total apparent energy today (auto-generated) 	Daily apparent energy 		kVAh
tapew (str)			Total apparent energy week (auto-generated) 	Weekly apparent energy 		kVAh
tapem (str)			Total apparent energy month (auto-generated) 	Monthly apparent energy 	kVAh
tapey (str)			Total apparent energy year (auto-generated) 	Yearly apparent energy 		kVAh

=================== ======================================================== ============================= ==============



**Faults**
-----------

Data group: faults
-------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			0
fault (arr)		Array of faults				[""]

=================== ========================================== =============================


	

**Warns**
---------

Data group: warns
-----------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			0
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================

	
