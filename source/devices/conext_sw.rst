

**Schneider Electric Conext SW**
#################################

**Device type: conext sw**


.. image:: ../images/conextsw.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: alternate text

**Device communication with ETRACK Gateway**

.. image:: ../images/conext_sw_line.png
	:width: 800px
   	:align: center
    	:height: 150px
    	:alt: alternate text


**Datapoints**
--------------

.. code-block:: JSON

   {
  "conext_sw": [
    {
      "sn": "",
      "addr": "",
      "batt": {
        "bv": "",
        "bc": "",
        "bp": "",
        "bt": "",
        "idc": "",
        "idp": "",
        "cdc": "",
        "cdp": "",
        "cdpp": "",
        "efbh": "",
        "efbt": "",
        "efbw": "",
        "efbm": "",
        "efby": "",
        "efbl": "",
        "etbh": "",
        "etbt": "",
        "etbw": "",
        "etbm": "",
        "etby": "",
        "etbl": "",
        "nbv": "",
        "bbc": ""
      },
      "grid": {
        "gcf": "",
        "gav": "",
        "gac": "",
        "gap": "",
        "gapa": "",
        "gaic": "",
        "gaip": "",
        "gal1v": "",
        "gal2v": "",
        "gal1c": "",
        "gal2c": "",
        "gieh": "",
        "giet": "",
        "giew": "",
        "giem": "",
        "giey": "",
        "giel": ""
      },
      "load": {
        "lav": "",
        "lal1v": "",
        "lal2v": "",
        "lac": "",
        "lal1c": "",
        "lal2c": "",
        "laf": "",
        "lap": "",
        "lapa": "",
        "loeh": "",
        "loet": "",
        "loew": "",
        "loem": "",
        "loey": "",
        "loel": ""
      },
      "gen": {
        "gav": "",
        "gac": "",
        "gaf": "",
        "gap": "",
        "gapa": "",
        "gieh": "",
        "giet": "",
        "giew": "",
        "giem": "",
        "giey": "",
        "giel": ""
      },
      "faults": {
        "n": "",
        "active": false,
        "fault": []
      },
      "warns": {
        "n": "",
        "active": true,
        "warn": []
      }
     }
    ]
   }



**Battery**
-----------
	
Data group: batt
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
bv (str)		Battery voltage					V
bc (str)		Battery current 				A
bp (str)		Battery power					W
bt (str)		Battery temperature				deg C
idc (str)		Invert DC current				A
idp (str)		Invert DC power					W
cdc (str)		Charge DC current				A
cdp (str)		Charge DC power					W
cdpp (str)		Charge DC power percentage			%
efbh (str)		Energy from battery hour			kWh
efbt (str)		Energy from battery today			kWh
efbw (str)		Energy from battery week			kWh
efbm (str)		Energy from battery month			kWh
efby (str)		Energy from battery year			kWh
efbl (str)		Energy from battery lifetime			kWh
etbh (str)		Energy to battery hour				kWh
etbt (str)		Energy to battery today				kWh
etbw (str)		Energy to battery week				kWh
etbm (str)		Energy to battery month				kWh
etby (str)		Energy to battery year				kWh
etbl (str)		Energy to battery lifetime			kWh
nbv (str)		Nominal Battery Voltage				V
bbc (str)		Battery Bank Capacity				Ah


=================== ========================================== ====================


**Grid (Light source coming from PHCN)**
-----------------------------------------

Data group: grid
----------------


=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
gcf (str)			Grid AC frequency			Hz
gav (str)			Grid AC voltage				V
gac (str)			Grid AC current				A
gap (str)			Grid AC power				W
gapa (str)			Grid AC power apparent			VA
gaic (str)			Grid AC input current			A
gaip (str)			Grid AC input power			W
gal1v (str)			Grid AC Line 1 Voltage			V
gal2v (str)			Grid AC Line 2 Voltage 			V
gal1c (str)			Grid AC Line 1 Current			A
gal2c (str)			Grid AC Line 2 Current			A
gieh (str)			Grid input energy hour			kWh
giet (str)			Grid input energy today			kWh
giew (str)			Grid input energy week			kWh
giem (str)			Grid input energy month			kWh
giey (str)			Grid input energy year			kWh
giel (str)			Grid input energy lifetime		kWh



=================== ========================================== ====================

**Load**
----------

Data group: load
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
lav (str)			Load AC voltage				V
lal1v (str)			Load AC Line 1 Voltage			V
lal2v (str)			Load AC Line 2 Voltage			V
lac (str)			Load AC current				A
lal1c (str)			Load AC Line 1 Current			A
lal2c (str)			Load AC Line 2 Current			A
laf (str)			Load AC frequency			Hz
lap (str)			Load AC power				W
lapa (str)			Load AC power apparent			VA
loeh (str)			Load output energy hour			kWh
loet (str)			Load output energy today		kWh
loew (str)			Load output energy week			kWh
loem (str)			Load output energy month		kWh
loey (str)			Load output energy year			kWh
loel (str)			Load output energy lifetime		kWh




=================== ========================================== ====================

**Generator**
-------------

Data group: gen
---------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
gav (str)			Gen AC voltage				V
gac (str)			Gen AC current				A
gaf (str)			Gen AC frequency			Hz
gap (str)			Gen AC power				W
gapa (str)			Gen AC power apparent			VA
gieh (str)			Gen input energy hour			kWh
giet (str)			Gen input energy today			kWh
giew (str)			Gen input energy week			kWh
giem (str)			Gen input energy month			kWh
giey (str)			Gen input energy year			kWh
giel (str)			Gen input energy lifetime		kWh



=================== ========================================== ====================


**Faults**
----------

Data group: faults
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			""
fault (arr)		Array of faults				[""]

=================== ========================================== =============================

**Warns**
----------

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			""
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================


