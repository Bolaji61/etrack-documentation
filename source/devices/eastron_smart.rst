**Eastron Smart x96**
######################
**Device type: eastron_smartx96_5j**


.. image:: ../images/eastronx96.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: alternate text


**Device communication with ETRACK Gateway**

.. image:: ../images/eastron_smart_line.png
	:width: 600px
   	:align: center
    	:height: 200px
    	:alt: alternate text



**Datapoints**
--------------

.. code-block:: JSON

   {
  "data": {
    "timestamp": 1562172417236,
    "compute": {
      "eastron_smartx96_5j": {
        "load": [
          {
            "use": "instant",
            "datapoint": "tse"
          },
          {
            "use": "change",
            "datapoint": "tape"
          },
          {
            "use": "change",
            "datapoint": "tace"
          },
          {
            "use": "change",
            "datapoint": "tree"
          }
        ]
      }
    },
    "devices": {
      "eastron_smartx96_5j": [
        {
          "load": {
            "p1lnv": "",
            "p2lnv": "",
            "p3lnv": "",
            "p1c": "",
            "p2c": "",
            "p3c": "",
            "p1acp": "",
            "p2acp": "",
            "p3acp": "",
            "p1app": "",
            "p2app": "",
            "p3app": "",
            "p1rep": "",
            "p2rep": "",
            "p3rep": "",
            "p1pf": "",
            "p2pf": "",
            "p3pf": "",
            "p1pa": "",
            "p2pa": "",
            "p3pa": "",
            "altnv": "",
            "alc": "",
            "slc": "",
            "tsp": "",
            "tspd": "",
            "mtspd": "",
            "iapd": "",
            "iapmd": "",
            "eapd": "",
            "eapmd": "",
            "tsapd": "",
            "mtsapd": "",
            "ncd": "",
            "mncd": "",
            "tsrepd": "",
            "mtsrepd": "",
            "p1dpf": "",
            "p2dpf": "",
            "p3dpf": "",
            "tdpf": "",
            "l1vcf": "",
            "l2vcf": "",
            "l3vcf": "",
            "l1ckf": "",
            "l2ckf": "",
            "l3ckf": "",
            "l1tl2v": "",
            "l2tl3v": "",
            "l3tl1v": "",
            "altlv": "",
            "nc": "",
            "p1ltnvthd": "",
            "p2ltnvthd": "",
            "p3ltnvthd": "",
            "p1cthd": "",
            "p2cthd": "",
            "p3cthd": "",
            "altnvthd": "",
            "alcthd": "",
            "p1cd": "",
            "p2cd": "",
            "p3cd": "",
            "mp1cd": "",
            "mp2cd": "",
            "mp3cd": "",
            "l1tl2vthd": "",
            "l2tl3vthd": "",
            "l3tl1vthd": "",
            "altlvthd": "",
            "tace": "",
            "tree": "",
            "l1iace": "",
            "l2iace": "",
            "l3iace": "",
            "l1eace": "",
            "l2eace": "",
            "l3eace": "",
            "l1tace": "",
            "l2tace": "",
            "l3tace": "",
            "l1iree": "",
            "l2iree": "",
            "l3iree": "",
            "l1eree": "",
            "l2eree": "",
            "l3eree": "",
            "l1tree": "",
            "l2tree": "",
            "l3tree": "",
            "v263hl1": "",
            "v263hl2": "",
            "v263hl3": "",
            "c263hl1": "",
            "c263hl2": "",
            "c263hl3": "",
            "vthl1": "",
            "vthl2": "",
            "vthl3": "",
            "cthl1": "",
            "cthl2": "",
            "cthl3": "",
            "tse": "",
            "tsapp": "",
            "tsrep": "",
            "tspf": "",
            "tspa": "",
            "fsv": "",
            "tiace": "",
            "teace": "",
            "tiree": "",
            "teree": "",
            "tape": ""
          },
          "faults": {
            "active": false,
            "fault": [],
            "n": 0
          },
          "sn": "92931501101378",
          "warns": {
            "active": false,
            "warn": [],
            "n": 0
          },
          "addr": "1.0"
        }
      ]
     }
    }
   }


**Load**
--------

Data group: load
-----------------

=================== ======================================================== ============================= ====================
Datapoints          	      Meaning                               						Unit
=================== ======================================================== ============================= ====================
p1lnv (str)			Phase 1 Line to Neutral Volts							V
p2lnv (str)			Phase 2 Line to Neutral Volts							V
p3lnv (str)			Phase 3 Line to Neutral Volts							V
p1c (str)			Phase 1 current									A
p2c (str)			Phase 2 current									A
p3c (str)			Phase 3 current									A
p1acp (str)			Phase 1 active power								W
p2acp (str)			Phase 2 active power								W
p3acp (str)			Phase 3 active power								W
p1app (str)			Phase 1 apparent power								VA
p2app (str)			Phase 2 apparent power								VA
p3app (str)			Phase 3 apparent power								VA
p1rep (str)			Phase 1 reactive power								VAr
p2rep (str)			Phase 2 reactive power								VAr
p3rep (str)			Phase 3 reactive power								VAr
p1pf (str)			Phase 1 power factor								None
p2pf (str)			Phase 2 power factor								None
p3pf (str)			Phase 3 power factor								None
p1pa (str)			Phase 1 phase angle								Degrees
p2pa (str)			Phase 2 phase angle								Degrees
p3pa (str)			Phase 3 phase angle								Degrees
altnv (str)			Average Line to Neutral Volts							V
alc (str)			Average line current								A
slc (str)			Sum of line current								A
tsp (str)			Total system power								W
tspd (str)			Total system power demand							W
mtspd (str)			Max total system power demand							W
iapd (str)			Import active power demand							W
iapmd (str)			Import active power max demand							W
eapd (str)			Export active power demand							W
eapmd (str)			Export active power max demand							W
tsapd (str)			Total system apparent demand							VA
mtsapd (str)			Max total system apparent demand						VA
ncd (str)			Neutral current demand								A
mncd (str)			Max neutral current demand							A
tsrepd (str)			Total system reactive power demand						VAr
mtsrepd (str)			Max total system reactive power demand						VAr
p1dpf (str)			Phase 1 displacement power factor						None
p2dpf (str)			Phase 2 displacement power factor						None
p3dpf (str)			Phase 3 displacement power factor						None
tdpf (str)			Total displacement power factor							None
l1vcf (str)			Line 1 Voltage Crest Factor							None
l2vcf (str)			Line 2 Voltage Crest Factor							None
l3vcf (str)			Line 3 Voltage Crest Factor							None
l1ckf (str)			Line 1 Current K Factor								None
l2ckf (str)			Line 2 Current K Factor								None
l3ckf (str)			Line 3 Current K factor								None
l1tl2v (str)			Line 1 To Line 2 Volts								V
l2tl3v (str)			Line 2 To Line 3 Volts								V
l3tl1v (str)			Line 3 To Line 1 Volts								V
altlv (str)			Average Line to Line Volts							V
nc (str)			Neutral Current									A
p1ltnvthd (str)			Phase 1 Line to Neutral Volts THD						%
p2ltnvthd (str)			Phase 2 Line to Neutral Volts THD						%
p3ltnvthd (str)			Phase 3 Line to Neutral Volts THD						%
p1cthd (str)			Phase 1 Current THD								%
p2cthd (str)			Phase 2 Current THD								%
p3cthd (str)			Phase 3 Current THD								%
altnvthd (str)			Average Line to Neutral Volts THD						%
alcthd (str)			Average Line Current THD							%
p1cd (str)			Phase 1 Current Demand								A
p2cd (str)			Phase 2 Current Demand								A
p3cd (str)			Phase 3 Current Demand								A
mp1cd (str)			Max Phase 1 Current Demand							A
mp2cd (str)			Max Phase 2 Current Demand							A
mp3cd (str)			Max Phase 3 Current Demand							A
l1tl2vthd (str)			Line 1 to Line 2 Volts THD							%
l2tl3vthd (str)			Line 2 to Line 3 Volts THD							%
l3tl1vthd (str)			Line 3 to Line 1 Volts THD							%
altlvthd (str)			Average Line to Line Volts THD							%
tace (str)			Total Active Energy								kWh
taceh (str)			Total Active Energy hour(auto-generated)	Hourly Active Energy 		kWh
taced(str)			Total Active Energy day(auto-generated)		Daily Active Energy		kWh
tacew (str)			Total Active Energy week(auto-generated)	Weekly Active Energy		kWh
tacem (str)			Total Active Energy Month(auto-generated)	Monthly Active Energy		kWh
tacey (str)			Total Active Energy Year (auto-generated)	Yearly Active Energy		kWh
tree (str)			Total Reactive Energy								kVarh
treeh (str)			Total Reactive Energy hour (auto-generated)	Hourly Reactive Energy 		kVarh
treed (str)			Total Reactive Energy day (auto-generated) 	Daily Reactive Energy  		kVarh
treew (str)			Total Reactive Energy Week (auto-generated)	Weekly Reactive Energy 		kVarh
treem (str)			Total Reactive Energy Month (auto-generated)	Monthly Reactive Energy 	kVarh
treey (str)			Total Reactive Energy Year (auto-generated)	Yearly Reactive Energy		kVarh
l1iace (str)			Line 1 import active energy							kWh
l2iace (str)			Line 2 import active energy							kWh
l3iace (str)			Line 3 import active energy							kWh
l1eace (str)			Line 1 export active energy							kWh
l2eace (str)			Line 2 export active energy							kWh
l3eace (str)			Line 3 export active energy							kWh
l1tace (str)			Line 1 total active energy							kWh
l2tace (str)			Line 2 total active energy							kWh
l3tace (str)			Line 3 total active energy							kWh
l1iree (str)			Line 1 import reactive energy							kVArh
l2iree (str)			Line 2 import reactive energy							kVArh
l3iree (str)			Line 3 import reactive energy							kVArh
l1eree (str)			Line 1 export reactive energy							kVArh
l2eree (str)			Line 2 export reactive energy							kVArh
l3eree (str)			Line 3 export reactive energy							kVArh
l1tree (str)			Line 1 Total Reactive energy 							kVArh
l2tree (str)			Line 2 Total Reactive energy							kVArh
l3tree (str)			Line 3 Total reactive energy							kVArh
v263hl1 (str)			Voltage 2st_63st harmonic Line 1						%
v263hl2 (str)			Voltage 2st_63st harmonic Line 2						%
v263hl3 (str)			Voltage 2st_63st harmonic Line 3						%
c263hl1 (str)			Current 2st_63st harmonic Line 1						%
c263hl2 (str)			Current 2st_63st harmonic Line 2						%
c263hl3 (str)			Current 2st_63st harmonic Line 3						%
vthl1 (str)			Voltage total harmonic Line 1							%
vthl2 (str)			Voltage total harmonic Line 2							%
vthl3 (str)			Voltage total harmonic Line 3							%
cthl1 (str)			Current total harmonic Line 1							%
cthl2 (str)			Current total harmonic Line 2							%
cthl3 (str)			Current total harmonic Line 3							%
tse (str)			Total system energy				Load Consumption		kWh
tseh (str)			Total system energy hour (auto-generated)	Hourly Load Consumption 	kWh
tsed (str)			Total system energy day (auto-generated) 	Daily Load Consumption  	kWh
tsew (str)			Total system energy week (auto-generated) 	Weekly Load Consumption  	kWh
tsem (str)			Total system energy month (auto-generated) 	Monthly Load Consumption 	kWh
tsey (str)			Total system energy year (auto-generated) 	Yearly Load Consumption  	kWh
tsapp (str)			Total system apparent power 							VA
tsrep (str)			Total system reactive power							VAr
tspf (str)			Total system power factor							None
tspa (str)			Total system phase angle							Degrees
fsv (str)			Frequency of supply voltages							Hz
tiace (str)			Total import active energy							kWh
teace (str)			Total export active energy							kWh
tiree (str)			Total import reactive energy							kVArh
teree (str)			Total export reactive energy							kVArh
tape (str)			Total apparent energy								kVAh
tapeh (str)			Total apparent energy hour (auto-generated)	Hourly Apparent Energy 		kVAh
taped (str)			Total apparent energy day (auto-generated) 	Daily Apparent Energy 		kVAh
tapew (str)			Total apparent energy week (auto-generated) 	Weekly Apparent Energy 		kVAh
tapem (str)			Total apparent energy month (auto-generated) 	Monthly Apparent Energy		kVAh
tapey (str)			Total apparent energy year (auto-generated) 	Yearly Apparent Energy 		kVAh



=================== ======================================================== ============================= ====================



**Faults**
----------

Data group: faults
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			0
fault (arr)		Array of faults				[""]

=================== ========================================== =============================

**Warns**
----------

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			0
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================


