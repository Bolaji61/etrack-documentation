**Voltronic Platform Inverter**
###############################

**Device type: voltronic inverter**


.. image:: ../images/voltronic.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: Voltronic


**Device communication with ETRACK Gateway**

.. image:: ../images/voltronic_line.png
	:width: 600px
   	:align: center
    	:height: 200px
    	:alt: alternate text


**Datapoints**
--------------

.. code-block:: JSON

   {
  "voltronic": [
    {
      "sn": "",
      "addr": "",
      "batt": {
        "bv": "",
        "bcc": "",
        "bdc": "",
        "bc": "",
        "bvscc"
      },
      "pv": {
        "pvic": "",
        "pviv": "",
        "pvcp": "",
        "pvce": "",
        "pvceh": "",  
        "pvced": "",  
        "pvcew": "",  
        "pvcem": "", 
        "pvcey": "",  
      },
      "load":{
        "acov": "",
        "acof": "",
        "acoap": "",
        "acop": "",
        "acoeh": "",  
        "acoed": "",  
        "acoew": "",  
        "acoem": "",  
        "acoey": "",  
        "acoe": "",
        "olp": ""
      },
      "grid":{
        "gv": "",
        "gf": ""
      },
      "faults": {
        "n": "",
        "active": false,
        "fault": []
      },
      "warns": {
        "n": "",
        "active": true,
        "warn": []
      }
    }
   ]
  }


**Battery**
-----------
	
Data group: batt
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
bv (str)			Battery Voltage				V
bcc (str)			Battery Charging Current		A
bdc (str)			Battery Discharge Current		A
bc (str)			Battery Capacity			%
bvscc (str)			Battery Voltage from SCC 		V


=================== ========================================== ====================


**Photo Voltaic**
-------------------

Data group: pv
--------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
pvic (str)			PV input current 			A
pviv (str)			PV input voltage 			V
pvcp (str)			PV charging power 			W
pvce (str)			PV charging energy			kWh

=================== ========================================== ====================

**Load**
--------

Data group: load
-----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
acov (str)			AC Output Voltage			V
acof (str)			AC Output Frequency			Hz
acoap (str)			AC Output Apparent Power		VA
acop (str)			AC Output Power				W
acoe (str)			AC Output Energy			kWh
olp (str)			Output Load Percentage			%

=================== ========================================== ====================

**Grid (Light source coming from PHCN)**
-----------------------------------------

Data group: grid
----------------

=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
gv (str)			Grid Voltage				V
gf (str)			Grid Frequency				Hz

=================== ========================================== ====================

**Faults**
----------

Data group: faults
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			""
fault (arr)		Array of faults				[""]

=================== ========================================== =============================

**Warns**
----------

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			" "
Warn (arr)		Array of warnings			[""]

=================== ========================================== =============================




