**Epever EpSolar**
####################

**Device type: epsolar_etracer**


.. image:: ../images/epsolar.jpg
	:width: 200px
   	:align: center
    	:height: 200px
    	:alt: alternate text

**Device communication with ETRACK Gateway**

.. image:: ../images/epsolar_line.png
	:width: 600px
   	:align: center
    	:height: 200px
    	:alt: alternate text

**Datapoints**
--------------

.. code-block:: JSON

   {
  "data": {
    "timestamp": 1562172417236,
    "compute": {
      "epsolar_etracer": {
        "pv": [
          "pvge"
        ]
      }
    },
    "devices": {
      "epsolar_etracer": [
        {
          "pv": {
            "pvarv": "0.0",
            "pvarc": "0.0",
            "pvarp": "0.0",
            "pvaiv": "0.0",
            "pvaic": "0.0",
            "pvaip": "0.0",
            "tie": "0.0",
            "mapvvt": "0.0",
            "mipvvt": "0.0",
            "get": "0.0",
            "gem": "0.0",
            "gey": "0.0",
            "tge": "0.0",
            "pvge": "0.0",
            "pvgeh": "0.0",
            "pvged": "0.0",
            "pvgew": "0.0",
            "pvgem": "0.0",
            "pvgey": "0.0"
          },
          "batt": {
            "brv": "0.0",
            "brc": "0.0",
            "brp": "0.0",
            "bp": "0.0",
            "bt": "0.0",
            "bsoc": "0.0",
            "rbt": "0.0",
            "brrp": "0.0",
            "mabvt": "0.0",
            "mibvt": "0.0",
            "bv": "0.0",
            "bc": "0.0",
            "bcap": "0.0"
          },
          "faults": {
            "active": false,
            "fault": [],
            "n": 0
          },
          "sn": "12345",
          "warns": {
            "active": false,
            "warn": [],
            "n": 0
          },
          "addr": "1.0"
        }
       ]
      }
     }
    }



**Photo Voltaic**
--------------------

Data group: pv
--------------------

=================== ================================================ ============================= ====================
Datapoints          	      Meaning                               					Unit
=================== ================================================ ============================= ====================
pvarv 			PV array rated voltage								V
pvarc			PV array rated current								A
pvarp			PV array rated power								W
pvaiv			PV array input voltage								V
pvaic			PV array input current								A
pvaip			PV array input power								W
tie			Temperature inside equipment							℃
mapvvt			Max PV voltage today								V
mipvvt			Min PV voltage today								V
get			Generated energy today								kWh
gem			Generated energy month								kWh
gey			Generated energy year								kWh
tge			Total generated energy								kWh
pvge			PV generated energy 								kWh
pvgeh			PV generated energy hour (auto-generated)    	Hourly Solar Generation 	kWh
pvged			PV generated energy day (auto-generated)    	Daily Solar Generation 		kWh
pvgew			PV generated energy week (auto-generated)   	Weekly Solar Generation 	kWh
pvgem			PV generated energy month (auto-generated)  	Monthly Solar Generation	kWh
pvgey			PV generated energy year (auto-generated)    	Yearly Solar Generation 	kWh


=================== ================================================ ============================= ====================


**Battery**
------------

Data group: batt
--------------------


=================== ========================================== ====================
Datapoints          	      Meaning                               Unit
=================== ========================================== ====================
brv			Battery rated voltage			V
brc			Battery rated current			A
brp			Battery rated power			W
bp			Battery power				W
bt			Battery temperature			℃
bsoc			Battery SOC (State of Charge)		%
rbt			Remote battery temperature		℃
brrp			Battery real rated power		V
mabvt			Max battery voltage today		V
mibvt			Min battery voltage today 		V
bv			Battery voltage				V
bc			Battery current				A
bcap			Battery capacity			AH

=================== ========================================== ====================



**Faults**
-----------

Data group: faults
--------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is fault present ?			true | false
n (str)			Number of active faults			0
fault (arr)		Array of faults				[""]

=================== ========================================== =============================


**Warns**
----------	

Data group: warns
------------------

=================== ========================================== =============================
Datapoints          	      Meaning                               	Unit
=================== ========================================== =============================
active (bool)		Is warning present ?			true | false
n (str)			Number of active warns			0
warn (arr)		Array of warnings			[""]

=================== ========================================== =============================


